# Drinks App

This is a single page app made with the following libs and frameworks:

- React + Typescript
- Node + Express
- Bootstrap
- Jest

## Getting started

The project is divided in two distinct folders, `server` and `web-app`. Inside of each one, run:

    - `npm install` to install all dependencies
    - `npm start` starts project
    - `npm test` run unit tests

## The concept

Once the server and client are running, visit `http://localhost:1234/` to see a list of events. It's possible to sort the listing by location (using Geolocation API) or by date.

## Special Notes

- Responsive design :iphone:
- Tested on latest versions of Chrome, FF and Safari

## TODOS

- [ ] create a `.env` file
- [ ] create more custom styles
- [ ] add e2e tests
