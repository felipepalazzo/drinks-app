const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

describe("Events", () => {
  it("/GET All events", async () => {
    const response = await request.get("/events");
    expect(response.status).toBe(200);
    expect(response.body[0]).toHaveProperty("id");
    expect(response.body[0]).toHaveProperty("title");
    expect(response.body[0]).toHaveProperty("type");
    expect(response.body[0]).toHaveProperty("time");
  });

  it("/GET/:id event", async () => {
    const response = await request.get("/events/1");
    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("id");
    expect(response.body).toHaveProperty("title");
    expect(response.body).toHaveProperty("type");
    expect(response.body).toHaveProperty("time");
  });
});
