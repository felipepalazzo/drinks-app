const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const helmet = require("helmet");
const eventRouter = require("./routes/event");

const app = express();

app.use(morgan("dev"));
app.use(helmet());
app.use(cors({ origin: "http://localhost:1234" }));
app.use("/events", eventRouter);

module.exports = app;
