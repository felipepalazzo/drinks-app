const sortByDistance = require("../sortByDistance");
const sortByDate = require("../sortByDate");

describe("helpers", () => {
  it("#sortByDistance", () => {
    const manchester = { latitude: 53.483959, longitude: -2.244644 };
    const amsterdam = { latitude: 52.370216, longitude: 4.895168 };
    const paris = { latitude: 48.8566, longitude: 2.3522 };

    const dataSet = [
      { location: amsterdam },
      { location: manchester },
      { location: paris },
    ];

    const london = { lat: 51.509865, lon: -0.118092 };
    expect(sortByDistance(dataSet, london)).toEqual([
      { location: manchester },
      { location: paris },
      { location: amsterdam },
    ]);
  });

  it("#sortByDate", () => {
    const events = [
      { time: "2018-06-23T20:00:00.000Z" },
      { time: "2017-06-23T20:00:00.000Z" },
      { time: "2017-05-23T20:00:00.000Z" },
    ];

    expect(events.sort(sortByDate)).toEqual([
      { time: "2017-05-23T20:00:00.000Z" },
      { time: "2017-06-23T20:00:00.000Z" },
      { time: "2018-06-23T20:00:00.000Z" },
    ]);
  });
});
