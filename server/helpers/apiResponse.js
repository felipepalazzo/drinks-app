exports.errorResponse = function (res, err) {
  return res.status(err.response.status).json({
    error: { msg: err.response.statusText },
  });
};
