const dayjs = require("dayjs");

function sortByDate(a, b) {
  return dayjs(a.time).isAfter(dayjs(b.time)) ? 1 : -1;
}

module.exports = sortByDate;
