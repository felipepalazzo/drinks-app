const EARTH_RAD_IN_KM = 6371;

function getDistance(lat1, lon1, lat2, lon2) {
  const p = Math.PI / 180;
  const c = Math.cos;
  const a =
    0.5 -
    c((lat2 - lat1) * p) / 2 +
    (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;

  return EARTH_RAD_IN_KM * 2 * Math.asin(Math.sqrt(a));
}

function sortByDistance(dataSet = [], coords = {}) {
  if ((!coords.lon && !coords.lat) || dataSet.length <= 0) {
    return [];
  }
  function sorting(event1, event2) {
    const d1 = getDistance(
      coords.lat,
      coords.lon,
      event1.location.latitude,
      event1.location.longitude
    );
    const d2 = getDistance(
      coords.lat,
      coords.lon,
      event2.location.latitude,
      event2.location.longitude
    );
    if (d1 < d2) {
      return -1;
    }
    if (d1 > d2) {
      return 1;
    }
    return 0;
  }

  return [...dataSet].sort(sorting);
}

module.exports = sortByDistance;
