const axios = require("axios").default;
const express = require("express");
const eventRouter = express.Router();

const sortByDate = require("../helpers/sortByDate");
const sortByDistance = require("../helpers/sortByDistance");
const apiResponse = require("../helpers/apiResponse");

const API_URL = "https://mock-api.drinks.test.siliconrhino.io/events";

eventRouter.get("/", async (req, res) => {
  const sortBy = req.query && req.query.sortBy;
  let response = {};
  try {
    response = await axios.get(API_URL);
    if (sortBy === "date") {
      response.data.sort(sortByDate);
    }
    if (sortBy === "local" && req.query.lat && req.query.lon) {
      const { lat, lon } = req.query;
      const sorted = sortByDistance(response.data, { lat, lon });
      return res.status(200).json(sorted);
    }
  } catch (err) {
    apiResponse.errorResponse(res, err);
  }

  res.status(200).json(response.data);
});

eventRouter.get("/:id", async (req, res) => {
  let response = {};
  try {
    response = await axios.get(`${API_URL}/${req.params.id}`);
  } catch (err) {
    apiResponse.errorResponse(res, err);
  }

  res.status(200).json(response.data);
});

module.exports = eventRouter;
