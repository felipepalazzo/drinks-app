import React, { useState, useReducer, useEffect } from 'react'
import api, { SortBy } from './api'
import appReducer, { Action, Status } from './reducer'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Cards, Spinner, Event, Dropdown } from './components'

export default function App() {
  const [app, dispatch] = useReducer(appReducer, { events: [] })
  const [sortBy, setSortBy] = useState(SortBy.Date)
  const [coords, setCoords] = useState(null)

  const handleClick = (sort: SortBy) => {
    setSortBy(sort)
  }

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(({ coords }) => {
        setCoords({ lat: coords.latitude, lon: coords.longitude })
      })
    }
  }, [])

  useEffect(() => {
    const fetchEvents = () => {
      dispatch({ type: Action.START })
      const opts: { sortBy: SortBy; coords?: { lon: string; lat: string } } = {
        sortBy,
      }
      if (sortBy === SortBy.Local && coords) {
        opts.coords = coords
      }
      api
        .get()
        .events(opts)
        .then(data => {
          dispatch({
            type: Action.DONE,
            payload: {
              events: data,
            },
          })
        })
        .catch(error => {
          dispatch({
            type: Action.FAIL,
            payload: { message: error.statusText },
          })
        })
    }

    fetchEvents()
  }, [sortBy])

  if (app.status === Status.Initial) {
    return <Spinner />
  }

  if (app.status === Status.Error) {
    return (
      <div className="alert alert-danger" role="alert">
        Sorry. Something went wrong.
      </div>
    )
  }

  return (
    <Router>
      <div data-testid="App container">
        <Switch>
          <Route exact path="/">
            <Dropdown active={sortBy} onClick={handleClick} />
            <Cards events={app.events} />
          </Route>
          <Route path="/events/:id">
            <Event />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}
