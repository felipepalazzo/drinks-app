import '@testing-library/jest-dom'
import { Router } from 'react-router-dom'
import { createMemoryHistory } from 'history'
import React from 'react'
import App from '../App'
import { render, waitFor, fireEvent } from '@testing-library/react'

jest.mock('../api', () => ({
  ...jest.requireActual('../api'),
  get: () => ({
    events: () =>
      new Promise(resolve =>
        resolve([
          {
            id: 1,
            title: 'Arkham Party',
            time: '2017-06-23T20:00:00.000Z',
            location: {
              name: 'Old Town',
            },
            guests: [{ name: 'Bruce Wayne' }],
            type: 'BEERS',
          },
        ])
      ),
    event: () =>
      new Promise(resolve =>
        resolve({
          title: 'Arkham Party',
          time: '2017-06-23T20:00:00.000Z',
          creator: {
            name: 'Alfred',
          },
          location: {
            name: 'Old Town',
          },
          guests: [{ name: 'Bruce Wayne' }],
          type: 'BEERS',
          comments: [
            {
              user: { name: 'Selina Kyle' },
              message: 'meow',
            },
          ],
        })
      ),
  }),
}))

describe('App', () => {
  test('it renders a list of events', async () => {
    const { getByTestId, getByText } = render(<App />)
    await waitFor(() => getByTestId('App container'))

    expect(getByText('Old Town')).toBeVisible()
    expect(getByText('Arkham Party')).toBeVisible()
    expect(getByText('23-6-2017')).toBeVisible()
  })
  test('it render a specific event', async () => {
    const history = createMemoryHistory()
    const { getByTestId, getByText } = render(
      <Router history={history}>
        <App />
      </Router>
    )
    await waitFor(() => getByTestId('App container'))

    fireEvent.click(getByText('More info'))

    await waitFor(() => getByTestId('Event detail'))

    expect(getByText('Old Town')).toBeVisible()
    expect(getByText('Arkham Party')).toBeVisible()
    expect(getByText('by Alfred')).toBeVisible()
    expect(getByText('Selina Kyle says:')).toBeVisible()
    expect(getByText('meow')).toBeVisible()
  })
})
