import { API_URL } from './constants'

export enum SortBy {
  Date = 'date',
  Local = 'local',
}

interface Options {
  sortBy?: SortBy
  coords?: { lon: string; lat: string }
}

export default {
  get: () => ({
    events: (opts: Options = {}) => {
      let baseUrl: string = `${API_URL}/events`
      if (opts.sortBy) {
        baseUrl += `?sortBy=${opts.sortBy}`
      }
      if (opts.coords) {
        baseUrl += `&lat=${opts.coords.lat}&lon=${opts.coords.lon}`
      }
      return fetch(baseUrl)
        .then(response => {
          if (response.ok) {
            return response.json()
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText,
            })
          }
        })
        .then(data => data)
    },
    event: (id: string) =>
      fetch(`${API_URL}/events/${id}`)
        .then(response => {
          if (response.ok) {
            return response.json()
          } else {
            return Promise.reject({
              status: response.status,
              statusText: response.statusText,
            })
          }
        })
        .then(data => data),
  }),
}
