import React from 'react'
import { Link } from 'react-router-dom'
import formatDate from '../helpers/formatDate'
import { chunk, capitalize } from 'lodash'
import { Event } from '../types'

interface Props {
  events: Event[]
}

const Cards: React.FC<Props> = ({ events }) => {
  const chunks = chunk(events, 3)

  return (
    <>
      {chunks.map((group: [], idx: number) => (
        <div key={idx} className="row">
          {group.map((event: Event) => (
            <div className="col-lg-4 col-sm-12" key={event.id}>
              <div className="card mb-4">
                <div className="card-body">
                  <h5 className="card-title">{event.title}</h5>
                  <p className="card-text text-muted">
                    <i className="fa fa-map-pin" aria-hidden="true"></i>
                    <span className="ml-1">{event.location.name}</span>
                  </p>
                  <p className="card-text text-muted">
                    <span className="mr-3">
                      <i className="fa fa-calendar" aria-hidden="true"></i>
                      <span className="ml-1">{formatDate(event.time)}</span>
                    </span>
                    <span className="mr-3">
                      <i className="fa fa-users" aria-hidden="true"></i>
                      <span className="ml-1">{event.guests.length}</span>
                    </span>
                    <span className="mr-3">
                      <i className="fa fa-beer" aria-hidden="true"></i>
                      <span className="ml-1">{capitalize(event.type)}</span>
                    </span>
                  </p>
                  <Link to={`events/${event.id}`} className="card-link">
                    More info
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
    </>
  )
}

export default Cards
