import React from 'react'
import { SortBy } from '../api'

interface Props {
  active: SortBy
  onClick: (sort: SortBy) => void
}

const Dropdown: React.FC<Props> = ({ active, onClick }) => {
  return (
    <div className="row pb-4">
      <div className="col-sm-6 offset-sm-6 text-right">
        <div className="dropdown">
          <button
            className="btn btn-primary dropdown-toggle"
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort By
          </button>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a
              className={`dropdown-item ${
                active === SortBy.Date ? 'active' : ''
              }`}
              href="#"
              onClick={() => onClick(SortBy.Date)}
            >
              Date
            </a>
            <a
              className={`dropdown-item ${
                active === SortBy.Local ? 'active' : ''
              }`}
              href="#"
              onClick={() => onClick(SortBy.Local)}
            >
              Location
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Dropdown
