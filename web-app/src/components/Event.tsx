import React, { useEffect, useState } from 'react'
import { capitalize } from 'lodash'
import { Link, useParams } from 'react-router-dom'
import formatDate from '../helpers/formatDate'
import Spinner from './Spinner'
import api from '../api'
import { EventComment } from '../types'

interface RouteParams {
  id: string
}

export default function Event() {
  let { id } = useParams<RouteParams>()
  const [event, setEvent] = useState(null)
  const [error, setError] = useState(false)

  useEffect(() => {
    const fetchEvent = () => {
      api
        .get()
        .event(id)
        .then(data => setEvent(data))
        .catch(() => {
          setError(true)
        })
    }

    fetchEvent()
  }, [])

  if (error) {
    return (
      <div className="alert alert-danger" role="alert">
        Sorry. Something went wrong.
      </div>
    )
  }

  if (!event) {
    return <Spinner />
  }

  return (
    <div data-testid="Event detail">
      <div className="row pb-4">
        <div className="col-sm-6">
          <Link to="/" className="text-secondary ml-1">
            <i className="fa fa-chevron-left" aria-hidden="true"></i>
            <span className="ml-1">Back</span>
          </Link>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title text-primary">{event.title}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                by {event.creator.name}
              </h6>
              <div className="pt-1 pb-1">
                <div>
                  <span className="font-weight-bold mr-2">Location:</span>
                  {event.location.name}
                </div>
                <div>
                  <span className="font-weight-bold mr-2">Date:</span>
                  {formatDate(event.time)}
                </div>
                <div>
                  <span className="font-weight-bold mr-2">Type:</span>
                  {capitalize(event.type)}
                </div>
                <div>
                  <span className="font-weight-bold mr-2">Guests:</span>
                  {event.guests.length} confirmed
                </div>
              </div>
            </div>
            {event.comments.length ? (
              <ul className="list-group list-group-flush">
                {event.comments.map((comment: EventComment, idx: number) => (
                  <li className="list-group-item" key={idx}>
                    <span className="font-italic text-muted">
                      {comment.user.name} says:
                    </span>{' '}
                    {comment.message}
                  </li>
                ))}
              </ul>
            ) : null}
          </div>
        </div>
      </div>
    </div>
  )
}
