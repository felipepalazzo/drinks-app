import formatDate from '../formatDate'

describe('formatDate', () => {
  test('date formating', () => {
    expect(formatDate('2017-06-23T20:00:00.000Z')).toEqual('23-6-2017')
  })
})
