import { Event } from './types'

export enum Action {
  START = 'START',
  DONE = 'DONE',
  FAIL = 'FAIL',
}

export enum Status {
  Initial,
  Success,
  Error,
}

interface State {
  status?: Status
  message?: string
  events: Event[]
}

export default function reducer(
  state: State,
  action: {
    type: Action
    payload?: {
      events?: Event[]
      message?: string
    }
  }
): State {
  switch (action.type) {
    case Action.START:
      return {
        ...state,
        status: Status.Initial,
      }
    case Action.DONE:
      return {
        ...state,
        status: Status.Success,
        events: action.payload.events,
      }
    case Action.FAIL:
      return {
        ...state,
        status: Status.Error,
        message: action.payload.message,
      }
    default:
      return { ...state }
  }
}
